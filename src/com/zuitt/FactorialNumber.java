package com.zuitt;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FactorialNumber {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num1 = 0;
        try {
            System.out.print("Input an integer whose factorial will be computed: ");
            num1 = in.nextInt();


            int i, result = 1;
            for (i = 1; i <= num1; i++) {
                result = result * i;
            }

            if (num1 < 0) {
                throw new Exception("Error");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a number.");
            return;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        int i, result = 1;
        for (i = 1; i <= num1; i++) {
            result = result * i;
        }
        System.out.println("The factorial of " + num1 + " is: " + result);
    }
}
